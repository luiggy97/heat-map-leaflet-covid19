const express = require('express');
const engine = require('ejs-mate');
const path = require('path');

//Initializations
const app = express();

//settings
app.engine('ejs', engine);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

//routes
app.use(require('./routes/'));

//start server
app.listen(3000,() =>{
    console.log('Sever on port 3000');
});

